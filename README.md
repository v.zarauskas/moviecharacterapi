# __Movie REST API__

## __Overview__

Spring Boot REST API with PostgreSQL implemented using Hibernate

## __Group Members__ ##

- Alexander Rol
- Nicolas Valentine
- Vytautas Zarauskas

## __Description__

The project is a Spring Boot REST API with PostgreSQL database implemented using Hibernate and deployed on Heroku. It contains franchises, movies and characters which all have the basic ***CRUD*** (Create, Read, Update and Delete) functions. Additional endpoints include the posibility to get ***all movies in a franchise***, ***all characters in a movie*** and ***all characters in a franchise***.

Some points of note:
- adhering to REST API conventions (including layering and versioning)
- optimizing business logic for best performance (including choices of architechture and data structures as well as their use)
- handling attempts of adding duplicate objects into the database and it's relations
- automated documentation using Swagger

## __Documentation__

Documentation was made using Swagger and can be found at: [documentation](https://movie-character-api-by-vytas.herokuapp.com/swagger-ui/#/)  

The publication can be found at: [movie-character-api](https://movie-character-api-by-vytas.herokuapp.com/)



[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.postman.co/run-collection/55de5b3a21146e098854)
