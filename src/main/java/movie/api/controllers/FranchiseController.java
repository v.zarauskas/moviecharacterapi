package movie.api.controllers;

import movie.api.models.Character;
import movie.api.models.Franchise;
import movie.api.models.Movie;
import movie.api.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

import static movie.api.controllers.ControllerHelper.BASE_URI_V1;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = BASE_URI_V1 + "/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseService franchiseService;

    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        return franchiseService.getAllFranchises();
    }

    @PostMapping
    public ResponseEntity<Franchise> createFranchise(@RequestBody Franchise newFranchise) {
        return franchiseService.createFranchise(newFranchise);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Franchise> getFranchiseById(@PathVariable long id) {
        return franchiseService.getFranchiseById(id);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Franchise> updateFranchiseById(@PathVariable long id, @RequestBody Franchise newFranchise) {
        return franchiseService.updateFranchiseById(id, newFranchise);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Franchise> deleteFranchiseById(@PathVariable long id) {
        return  franchiseService.deleteFranchiseById(id);
    }

    @PutMapping(value = "/{franchiseId}/add/movie/{movieId}")
    public ResponseEntity<Franchise> addMovieByIdToFranchiseById(@PathVariable long movieId, @PathVariable long franchiseId) {
        return franchiseService.addMovieByIdToFranchiseById(movieId, franchiseId);
    }

    @GetMapping(value = "/{id}/characters")
    public ResponseEntity<Set<Character>> getAllFranchiseCharactersByFranchiseId(@PathVariable long id) {
        return franchiseService.getAllFranchiseCharactersByFranchiseId(id);
    }

    @GetMapping(value = "/{id}/movies")
    public ResponseEntity<Set<Movie>> getAllFranchiseMoviesByFranchiseId(@PathVariable long id) {
        return franchiseService.getAllFranchiseMoviesByFranchiseId(id);
    }
}
