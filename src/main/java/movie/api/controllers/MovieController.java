package movie.api.controllers;

import movie.api.models.Character;
import movie.api.models.Movie;
import movie.api.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

import static movie.api.controllers.ControllerHelper.BASE_URI_V1;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = BASE_URI_V1 + "/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        return new ResponseEntity<>(movieService.getAllMovies(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Movie> createMovie(@RequestBody Movie newMovie) {
        return movieService.createMovie(newMovie);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable long id) {
        return movieService.getMovieById(id);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Movie> updateMovieById(@PathVariable long id, @RequestBody Movie newMovie) {
        return movieService.updateMovieById(id, newMovie);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Movie> deleteMovieById(@PathVariable long id) {
        return movieService.deleteMovieById(id);
    }

    @PutMapping(value = "/{movieId}/add/character/{characterId}")
    public ResponseEntity<Movie> addCharacterByIdToMovieById(@PathVariable long characterId, @PathVariable long movieId) {
        return movieService.addCharacterByIdToMovieById(characterId, movieId);
    }

    @GetMapping(value = "/{id}/characters")
    public ResponseEntity<Set<Character>> getAllCharactersByMovieId(@PathVariable long id) {
        return movieService.getAllCharactersByMovieId(id);
    }
}