package movie.api.controllers;

import movie.api.models.Character;
import movie.api.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static movie.api.controllers.ControllerHelper.BASE_URI_V1;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = BASE_URI_V1 + "/characters")
public class CharacterController {

    @Autowired
    CharacterService characterService;

    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters() {
        return new ResponseEntity<>(characterService.getAllCharacters(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Character> createCharacter(@RequestBody Character newCharacter) {
        return characterService.createCharacter(newCharacter);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable long id) {
        return characterService.getCharacterById(id);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Character> updateCharacterById(@PathVariable long id, @RequestBody Character newCharacter) {
        return characterService.updateCharacterById(id, newCharacter);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Character> deleteCharacterById(@PathVariable long id) {
        return characterService.deleteCharacterById(id);
    }
}
