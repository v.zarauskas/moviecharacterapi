package movie.api.services;

import movie.api.models.Character;
import movie.api.models.Gender;
import movie.api.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterService {

    @Autowired
    private CharacterRepository characterRepository;

    public List<Character> getAllCharacters() {
        return characterRepository.findAll();
    }

    public ResponseEntity<Character> createCharacter(Character newCharacter) {
        if (characterRepository.findByFullNameAndAliasAndGender(newCharacter.getFullName(), newCharacter.getAlias(),
                newCharacter.getGender()) == null) {
            return new ResponseEntity<>(characterRepository.save(newCharacter), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Character> getCharacterById(long id) {
        Character returnCharacter = new Character();
        HttpStatus status;
        if (existsById(id)) {
            returnCharacter = findById(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacter, status);
    }

    public ResponseEntity<Character> updateCharacterById(long id, Character newCharacter) {
        HttpStatus status;
        if (existsById(id)) {
            Character character = findById(id);

            String newFullName = newCharacter.getFullName();
            character.setFullName(newFullName);

            String newAlias = newCharacter.getAlias();
            character.setAlias(newAlias);

            Gender newGender = newCharacter.getGender();
            character.setGender(newGender);

            String newCharacterPictureSrc = newCharacter.getCharacterPictureSrc();
            character.setCharacterPictureSrc(newCharacterPictureSrc);

            characterRepository.save(character);
            status = HttpStatus.OK;
            return new ResponseEntity<>(character, status);
        } else {
            createCharacter(newCharacter);
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(newCharacter, status);
        }
    }

    public ResponseEntity<Character> deleteCharacterById(long id) {
        HttpStatus status;
        if (characterRepository.existsById(id)) {
            characterRepository.deleteById(id);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(null, status);
    }

    public boolean existsById(long id) {
        return characterRepository.existsById(id);
    }

    public Character findById(long id) {
        return characterRepository.findById(id).get();
    }
}
