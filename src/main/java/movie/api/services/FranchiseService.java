package movie.api.services;

import movie.api.models.Character;
import movie.api.models.Franchise;
import movie.api.models.Movie;
import movie.api.repositories.FranchiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class FranchiseService {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieService movieService;
    
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    public ResponseEntity<Franchise> createFranchise(Franchise newFranchise) {
        if (franchiseRepository.findByFranchiseName(newFranchise.getFranchiseName()) == null) {
            return new ResponseEntity<>(franchiseRepository.save(newFranchise), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Franchise> getFranchiseById(long id) {
        Franchise franchise = new Franchise();
        HttpStatus status;
        if (existsById(id)) {
            franchise = findById(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(franchise, status);
    }

    public ResponseEntity<Franchise> updateFranchiseById(long id, Franchise newFranchise) {
        HttpStatus status;
        if (existsById(id)) {
            Franchise franchise = findById(id);

            String updateFranchiseName = newFranchise.getFranchiseName();
            franchise.setFranchiseName(updateFranchiseName);

            String updateDescription = newFranchise.getDescription();
            franchise.setDescription(updateDescription);

            franchiseRepository.save(franchise);
            status = HttpStatus.OK;
            return new ResponseEntity<>(franchise, status);
        } else {
            createFranchise(newFranchise);
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(newFranchise, status);
        }
    }

    public ResponseEntity<Franchise> deleteFranchiseById(long id) {
        HttpStatus status;
        if (existsById(id)) {
            franchiseRepository.deleteById(id);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(null, status);
    }

    public ResponseEntity<Franchise> addMovieByIdToFranchiseById(long movieId, long franchiseId) {
        Franchise franchise = new Franchise();
        HttpStatus status;
        boolean movieExists = movieService.existsById(movieId);
        boolean franchiseExists = franchiseRepository.existsById(franchiseId);
        if (movieExists) {
            if (franchiseExists) {
                franchise = findById(franchiseId);
                Movie movie = movieService.findById(movieId);
                movie.setFranchise(franchise);
                franchise.addMovie(movie);
                franchiseRepository.save(franchise);
                movieService.updateMovieById(movieId, movie);
                status = HttpStatus.OK;
                // The franchise does not exist
            } else {
                status = HttpStatus.NOT_FOUND;
            }
            // The movie does not exist
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(franchise, status);
    }

    public ResponseEntity<Set<Character>> getAllFranchiseCharactersByFranchiseId(long id) {
        Set<Character> characters = new HashSet<>();
        HttpStatus status;
        if (existsById(id)) {
            Franchise franchise = findById(id);
            characters = franchise.getCharacters();
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(characters, status);
    }

    public ResponseEntity<Set<Movie>> getAllFranchiseMoviesByFranchiseId(long id) {
        Set<Movie> movies = new HashSet<>();
        HttpStatus status;
        if (existsById(id)) {
            Franchise franchise = findById(id);
            movies = franchise.getMovies();
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movies, status);
    }

    public boolean existsById(long id) {
        return franchiseRepository.existsById(id);
    }

    public Franchise findById(long id) {
        return franchiseRepository.findById(id).get();
    }
}
