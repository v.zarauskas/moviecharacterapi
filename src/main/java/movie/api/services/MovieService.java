package movie.api.services;

import movie.api.models.Character;
import movie.api.models.Movie;
import movie.api.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MovieService {
    
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CharacterService characterService;

    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    public ResponseEntity<Movie> createMovie(Movie newMovie) {
        if (movieRepository.findByMovieTitleAndDirectorAndReleaseYear(newMovie.getMovieTitle(), newMovie.getDirector(), newMovie.getReleaseYear()) == null) {
            return new ResponseEntity<>(movieRepository.save(newMovie), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Movie> getMovieById(long id) {
        Movie returnMovie = new Movie();
        HttpStatus status;
        if (existsById(id)) {
            returnMovie = findById(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, status);
    }
    public ResponseEntity<Movie> updateMovieById(long id, Movie newMovie) {
        HttpStatus status;
        if (existsById(id)) {
            Movie movie = findById(id);

            String newMovieTitle = newMovie.getMovieTitle();
            movie.setMovieTitle(newMovieTitle);

            String newDirector = newMovie.getDirector();
            movie.setDirector(newDirector);

            int newReleaseYear = newMovie.getReleaseYear();
            movie.setReleaseYear(newReleaseYear);

            String newGenres = newMovie.getGenres();
            movie.setGenres(newGenres);

            String newMoviePictureSrc = newMovie.getMoviePictureSrc();
            movie.setMoviePictureSrc(newMoviePictureSrc);

            String newTrailerURI = newMovie.getTrailerURI();
            movie.setTrailerURI(newTrailerURI);

            movieRepository.save(movie);
            status = HttpStatus.OK;
            return new ResponseEntity<>(movie, status);
        } else {
            createMovie(newMovie);
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(newMovie, status);
        }
    }

    public ResponseEntity<Movie> deleteMovieById(long id) {
        HttpStatus status;
        if (existsById(id)) {
            movieRepository.deleteById(id);
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(null, status);
    }

    public ResponseEntity<Movie> addCharacterByIdToMovieById(long characterId, long movieId) {
        Movie movie = new Movie();
        HttpStatus status;
        boolean characterExists = characterService.existsById(characterId);
        boolean movieExists = existsById(movieId);
        if (characterExists) {
            if (movieExists) {
                movie = findById(movieId);
                boolean movieAssignedToFranchise = movie.getFranchise() != null;
                // The check below is necessary because during assignment of a character to a movie, it also has to be assigned to a franchise for the functionality of the app
                if (movieAssignedToFranchise) {
                    Character character = characterService.findById(characterId);
                    movie.addCharacter(character);
                    character.setFranchise(movie.getFranchise());
                    movieRepository.save(movie);
                    characterService.updateCharacterById(characterId, character);
                    status = HttpStatus.OK;
                // The movie is not assigned to a franchise
                } else {
                    status = HttpStatus.NOT_FOUND;
                }
            // The movie does not exist
            } else {
                status = HttpStatus.NOT_FOUND;
            }
        // The character does not exist
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movie, status);
    }

    public ResponseEntity<Set<Character>> getAllCharactersByMovieId(long id) {
        Set<Character> characters = new HashSet<>();
        HttpStatus status;
        if (existsById(id)) {
            Movie movie = findById(id);
            characters = movie.getCharacters();
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(characters, status);
    }

    public boolean existsById(long id) {
        return movieRepository.existsById(id);
    }

    public Movie findById(long id) {
        return movieRepository.findById(id).get();
    }
}
