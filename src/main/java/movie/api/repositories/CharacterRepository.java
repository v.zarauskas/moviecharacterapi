package movie.api.repositories;

import movie.api.models.Character;
import movie.api.models.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
    Character findByFullNameAndAliasAndGender(String fullName, String alias, Gender gender);
}
